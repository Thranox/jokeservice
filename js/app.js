const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;
const GET = require('../js/HTTP-Requests/GET');
const express = require('express');
const app = express();

mongoose.Promise = Promise;

const jokes = new Schema({
    setup: String,
    punchline: String
});

const Jokes = mongoose.model('My jokes', jokes);

function createJoke(setup, punchline) {
    let newJoke = new Jokes({
        setup,
        punchline
    });
    return newJoke.save();
}

app.get('/api/jokes', async(req, res)=>{
    const response = await Jokes.find();
    res.send(response);
});

//CONNECTS TO DB
mongoose.connect('mongodb+srv://William:williamogsimon@cluster0-dhoif.gcp.mongodb.net/Jokes', {useNewUrlParser: true});

async function createDB() {
    let testJoke = await createJoke('this is setup','this is punchline');
    console.log(testJoke);
}

app.listen(8015);
createDB();
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

mongoose.Promise = Promise;

const jokes = new Schema({
    setup: String,
    punchline: String
});

const Jokes = mongoose.model('My jokes', jokes);

function createJoke(setup, punchline) {
    let newJoke = new Jokes({
        setup,
        punchline
    });
    return newJoke.save();
}
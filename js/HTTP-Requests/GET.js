const fetch = require('node-fetch');

async function GET(url){
    let OK = 200;
    const response = await fetch(url);
        if(response.status !== OK)
            throw new Error('GET status code' + response.status);
        return await response.json();
}

module.exports = GET;